
var HelloMessage = React.createClass ({
	render: function() {
		var name;
		if (this.props.name)
			name = this.props.name;
		else
			name = this.props.params.name;
		return (<div>Hello {name}</div>)
	}
});

var Timer = React.createClass ({
	contextTypes: {
	    router: React.PropTypes.object.isRequired
	  },
	  
	getInitialState: function() {
		return {
			secondsElapsed: 0
		}
	},

	tick: function() {		
		this.setState(function (prevState) {
			return {secondsElapsed: prevState.secondsElapsed + 1};
		});
		// redirects after 5 seconds		
		if (this.state.secondsElapsed > 5) {
			this.context.router.replace("/hello/redirected");
		}
	},

	componentDidMount: function () {
		this.interval = setInterval(() => this.tick(), 1000);
	},

	componentWillUnmount() {
		clearInterval(this.interval);
	},

	render: function() {
		return (
				<div>
				<HelloMessage name="Jane" />
					Seconds elapsed since you arrived: {this.state.secondsElapsed}
				</div>
		)
	}
});

var TodoApp = React.createClass ({
	getInitialState: function() {		
		return {items: [], text: '', done: 0};
	},

	render: function() {
		return (
			<div>
				<Link to={`/timer`}>Click to go to timer app</Link><br/>
				<Link to={`/hello/CIAI`}>Click to go to hello app</Link>
				<h3>TODO - done: {this.state.done}</h3>
				<TodoList items={this.state.items} updateDone={this.handleUpdateDone} />
				<form onSubmit={this.handleSubmit}>
					<input onChange={this.handleChange} value={this.state.text} />
					<button>{'Add #' + (this.state.items.length + 1) + ': ' + this.state.text}</button>
				</form>
			</div>
		);
	},

	handleChange(e) {
		this.setState({text: e.target.value});
	},

	handleSubmit(e) {
		e.preventDefault();
		var newItem = {
				text: this.state.text,
				id: Date.now()
		};
		this.setState((prevState) => ({
			items: prevState.items.concat(newItem),
			text: ''
		}));
	},
	
	handleUpdateDone: function (checked) {
		this.setState(
				function (prevState) {
					return {done: prevState.done + checked};
				}
		)
	}
});

var TodoList = React.createClass({
	handleItemClick: function(e) {
		if (e.target.checked)
			this.props.updateDone(1);
		else
			this.props.updateDone(-1);
	},

	render: function() {
		return (
				<ul>
				{this.props.items.map(item => (
						<li key={item.id}>
							<input type="checkbox" key={"done"+item.id} 
								onClick={this.handleItemClick} />
							{item.text}
						</li>
				))}
				</ul>
		);
	}
});

var Link = ReactRouter.Link;
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;

ReactDOM.render((
		<Router>
	    	<Route path="/" component={TodoApp}/>
	    	<Route path="/timer" component={Timer}/>
	    	<Route path="/hello/:name" component={HelloMessage}/>
	    </Router>
		), document.getElementById('react_content'))
